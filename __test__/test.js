const server = require('../server.js')
const supertest = require('supertest')
const { response } = require('../server.js')
const requestWithSupertest = supertest(server)

describe('Coach and Player Endpoints', () => {
	// test GET ALL STATISTICS chadha_hajji_crudStat.js
	it('GET api/statistics should show all statistics', async () => {
		const res = await requestWithSupertest.get('/api/statistics')
		expect(res.status).toEqual(200)
	})
	// test ADD COMPETENCE chadha_hajji_crudComp.js
	it('POST /api/competence should add a competence', async () => {
		const competence = {
			name: 'Running',
			description: 'Run 12km',
			link: 'www.youtube.com',
			visibility: 'true',
			stars: 3,
		}
		const competenceRes = await requestWithSupertest
			.post('/api/competence')
			.send(competence)

		expect(competenceRes.statusCode).toEqual(200)
		const res = await requestWithSupertest.get(`/api/competence/${competenceRes.body._id}`)
		expect(competenceRes.body.name).toEqual('Running')
	})
	// test ADD STATISTIC chadha_hajji_crudStat.js
	it('POST /api/statistic should add a statistic', async () => {
		const statistic = {
			type: 'Timer',
			unit: 'km',
			title: 'Squats',
			description: 'Do 100 squats',
			link: 'www.fitnessApp.com',
			visibility: 'true',
			currentState: '56',
			minMax: 'Minimiser',
			statAlert: 'false',
		}
		const statisticRes = await requestWithSupertest.post('/api/statistic').send(statistic)

		expect(statisticRes.statusCode).toEqual(200)
		const res = await requestWithSupertest.get(`/api/statistic/${statisticRes.body._id}`)
		expect(statisticRes.body.title).toEqual('Squats')
	})
	// test Invite Player chadha_hajji_invitePlayer.js
})
